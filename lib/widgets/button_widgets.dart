import 'package:flutter/material.dart';
import 'package:get/get_rx/src/rx_typedefs/rx_typedefs.dart';

class ButtonWidget extends StatelessWidget {
  final String name;
  final Callback onPress;
  const ButtonWidget({
    Key? key,
    required this.name,
    required this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      width: 300,
      child: TextButton(
        onPressed: onPress,
        child: Text(
          name,
          style: const TextStyle(
            color: Colors.white,
          ),
        ),
        style: TextButton.styleFrom(
          shadowColor: Colors.grey.withOpacity(0.8),
          elevation: 5,
          backgroundColor: Colors.blue,
        ),
      ),
    );
  }
}
