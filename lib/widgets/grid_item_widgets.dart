import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get_rx/src/rx_typedefs/rx_typedefs.dart';
import 'package:google_fonts/google_fonts.dart';

class GridItemWidget extends StatelessWidget {
  final String name;
  final String description;
  final int price;
  final String image;
  final Callback onPress;
  final Icon icon;
  final double rating;
  const GridItemWidget({
    Key? key,
    required this.name,
    required this.description,
    required this.price,
    required this.image,
    required this.onPress,
    required this.icon,
    required this.rating,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 5, right: 5),
      padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.all(Radius.circular(8)),
          border: Border.all(color: Colors.blue, width: 2),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.8),
              //spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, 8),
            ),
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Hero(
            tag: name,
            child: CachedNetworkImage(
              height: 150,
              width: 150,
              imageUrl: image,
              placeholder: (context, url) => const Center(
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  color: Colors.teal,
                ),
              ),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            name,
            style: GoogleFonts.faunaOne(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.black),
          ),
          SizedBox(
            width: double.infinity,
            height: 40,
            child: Text(
              description,
              textAlign: TextAlign.center,
              style: const TextStyle(fontSize: 12, color: Colors.black38),
            ),
          ),
          RatingBar.builder(
            itemSize: 20,
            initialRating: 3.5,
            minRating: 1,
            direction: Axis.horizontal,
            allowHalfRating: true,
            itemCount: 5,
            itemPadding: const EdgeInsets.symmetric(horizontal: 1),
            itemBuilder: (context, _) => const Icon(
              Icons.star,
              color: Colors.amber,
            ),
            onRatingUpdate: (rating) {
              print(rating);
            },
          ),
          Row(
            children: [
              Text(
                "\$${price.toDouble()}",
                style: GoogleFonts.denkOne(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.blue),
              ),
              const Expanded(child: SizedBox()),
              TextButton(
                onPressed: onPress,
                child: const Text(
                  "ADD",
                  style: TextStyle(color: Colors.white),
                ),
                style: TextButton.styleFrom(backgroundColor: Colors.green),
              )
            ],
          ),
        ],
      ),
    );
  }
}
