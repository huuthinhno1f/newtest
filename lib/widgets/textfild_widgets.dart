import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextFildWidget extends StatelessWidget {
  final String name;
  final Icon icon;
  final bool hidepass;
  final TextEditingController controller;
  final TextInputType inputType;
  const TextFildWidget({
    Key? key,
    required this.name,
    required this.icon,
    required this.controller,
    this.hidepass = false,
    this.inputType = TextInputType.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 10),
      width: 300,
      height: 50,
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.4),
          //spreadRadius: 5,
          blurRadius: 7,
          offset: const Offset(0, 8),
        )
      ]),
      child: TextField(
        keyboardType: inputType,
        controller: controller,
        obscureText: hidepass,
        decoration: InputDecoration(
            hintText: name,
            icon: icon,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            focusedErrorBorder: InputBorder.none,
            border: InputBorder.none),
      ),
    );
  }
}
