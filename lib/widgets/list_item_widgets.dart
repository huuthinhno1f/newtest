import 'package:animated_card/animated_card.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_typedefs/rx_typedefs.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testthinh/controllers/authen_controllers.dart';
import 'package:testthinh/controllers/store_controllers.dart';

class ListItemWidget extends StatelessWidget {
  final String name;
  final String description;
  final int price;
  final String image;
  final Callback onPress;
  final Icon icon;
  final String id;
  //final double rating;
  ListItemWidget({
    Key? key,
    required this.name,
    required this.description,
    required this.price,
    required this.image,
    required this.onPress,
    required this.icon,
    required this.id,

    //required this.rating,
  }) : super(key: key);
  final authenController = Get.put(AuthenController());
  final storeController = Get.put(StoreController());
  @override
  Widget build(BuildContext context) {
    return AnimatedCard(
      direction: AnimatedCardDirection.top, //Initial animation direction
      initDelay: const Duration(milliseconds: 0), //Delay to initial animation
      duration: const Duration(milliseconds: 400), //Initial animation duration
      curve: Curves.easeInBack,
      onRemove: () {
        storeController.deleteCart(id);
        EasyLoading.showToast("Đã xoá $name");
        storeController.getStoreCart();
      },

      child: Container(
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
            border: Border.all(color: Colors.blue, width: 2),
            borderRadius: const BorderRadius.all(Radius.circular(5)),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.8),
                //spreadRadius: 5,
                blurRadius: 7,
                offset: const Offset(0, 8),
              )
            ]),
        width: double.infinity,
        height: 160,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: Hero(
                tag: name,
                child: CachedNetworkImage(
                  imageUrl: image,
                  placeholder: (context, url) => const CircularProgressIndicator(
                    strokeWidth: 2,
                    color: Colors.teal,
                  ),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
              ),
            ),
            const SizedBox(
              width: 20,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    name,
                    style: GoogleFonts.creteRound(
                      fontSize: 26,
                      fontWeight: FontWeight.bold,
                      color: Colors.blue,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(description),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const Text(
                        "Price",
                        style: TextStyle(
                          color: Colors.green,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text(
                        "$price\$",
                        style: GoogleFonts.corben(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  RatingBar.builder(
                    itemSize: 20,
                    initialRating: 3.5,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemPadding: const EdgeInsets.symmetric(horizontal: 1),
                    itemBuilder: (context, _) => const Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                ],
              ),
            ),
            IconButton(onPressed: () {}, icon: const Icon(Icons.add)),
            Obx(() => const Text(
                  "1",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                )),
            IconButton(onPressed: () {}, icon: const Icon(Icons.remove)),
            IconButton(onPressed: onPress, icon: icon),
            const SizedBox(
              width: 20,
            ),
          ],
        ),
      ),
    );
  }
}
