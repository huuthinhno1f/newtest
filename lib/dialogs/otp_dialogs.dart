import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:testthinh/widgets/button_widgets.dart';

class OtpDialog extends StatefulWidget {
  const OtpDialog({Key? key}) : super(key: key);

  @override
  State<OtpDialog> createState() => _OtpDialogState();
}

class _OtpDialogState extends State<OtpDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        content: SizedBox(
      height: 400,
      width: 500,
      child: Column(
        children: [
          Text("OTP",
              style: GoogleFonts.crimsonPro(fontWeight: FontWeight.bold, fontSize: 60, color: Colors.blue, shadows: [
                const Shadow(
                  offset: Offset(4, 6),
                  blurRadius: 3.0,
                  color: Colors.black,
                ),
              ])),
          const SizedBox(
            height: 30,
          ),
          Text(
            "Phone Number Verification",
            style: GoogleFonts.elsieSwashCaps(fontWeight: FontWeight.bold, fontSize: 22),
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Enter the code sent to: ",
                style: GoogleFonts.convergence(fontSize: 16),
              ),
              Text(
                "+84 868789402",
                style: GoogleFonts.convergence(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          PinCodeTextField(
            length: 6,
            obscureText: false,
            animationType: AnimationType.fade,
            pinTheme: PinTheme(
              shape: PinCodeFieldShape.box,
              borderRadius: BorderRadius.circular(5),
              fieldHeight: 50,
              fieldWidth: 40,
              activeFillColor: Colors.white,
            ),
            animationDuration: const Duration(milliseconds: 300),
            backgroundColor: Colors.blue.shade50,
            enableActiveFill: true,
            // errorAnimationController: errorController,
            // controller: textEditingController,
            onCompleted: (v) {
              print("Completed");
            },
            onChanged: (value) {
              print(value);
              setState(() {
                // currentText = value;
              });
            },
            beforeTextPaste: (text) {
              print("Allowing to paste $text");
              //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
              //but you can show anything you want here, like your pop up saying wrong paste format or etc
              return true;
            },
            appContext: context,
          ),
          const SizedBox(
            height: 30,
          ),
          ButtonWidget(name: "VERIFY", onPress: () {})
        ],
      ),
    ));
  }
}
