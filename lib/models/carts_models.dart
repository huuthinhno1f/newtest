class CartModel {
  String? userId;
  String? name;
  int? price;
  String? image;
  String? description;
  //int? amount;

  CartModel({this.userId, this.name, this.price, this.image, this.description});
  factory CartModel.fromJson(Map<String, dynamic> json) => CartModel(
        userId: json["userId"],
        name: json["name"],
        price: json["price"],
        image: json["image"],
        description: json["description"],
        //amount: json["amount"],
      );
}
