class ProductModel {
  String? name;
  int? price;
  String? image;
  String? description;
  //int? amount;

  ProductModel({this.name, this.price, this.image, this.description});
  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        name: json["name"],
        price: json["price"],
        image: json["image"],
        description: json["description"],
        //amount: json["amount"],
      );
}
