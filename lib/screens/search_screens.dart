import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testthinh/controllers/store_controllers.dart';
import 'package:testthinh/widgets/grid_item_widgets.dart';

import 'information_screens.dart';

class SearchScreens extends StatefulWidget {
  const SearchScreens({Key? key}) : super(key: key);

  @override
  _SearchScreensState createState() => _SearchScreensState();
}

class _SearchScreensState extends State<SearchScreens> {
  TextEditingController txtSearchController = TextEditingController();
  String search = "";
  final storeController = Get.put(StoreController());
  @override
  Widget build(BuildContext context) {
    final Stream<QuerySnapshot> _usersStream = search != ""
        ? FirebaseFirestore.instance.collection('Product').where("key", arrayContains: search).snapshots()
        : FirebaseFirestore.instance.collection('Product').snapshots();
    return SingleChildScrollView(
      child: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(left: 10),
              width: 300,
              height: 50,
              decoration: BoxDecoration(border: Border.all(color: Colors.black)),
              child: TextField(
                controller: txtSearchController,
                onChanged: (value) {
                  setState(() {
                    //txtSearchController.text = value;
                    search = value.toLowerCase();
                    print(search);
                  });
                },
                decoration: const InputDecoration(
                    hintText: "Search...",
                    icon: Icon(Icons.search),
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    focusedErrorBorder: InputBorder.none,
                    border: InputBorder.none),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            SizedBox(
              width: double.infinity,
              height: 800,
              child: StreamBuilder<QuerySnapshot>(
                stream: _usersStream,
                builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasError) {
                    return const Text('Something went wrong');
                  }

                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(
                      child: CircularProgressIndicator(
                        color: Colors.tealAccent,
                        strokeWidth: 2,
                      ),
                    );
                  }

                  return RefreshIndicator(
                    onRefresh: () {
                      return Future.delayed(
                        const Duration(seconds: 1),
                        () {},
                      );
                    },
                    child: GridView.count(
                      physics: const BouncingScrollPhysics(),
                      crossAxisCount: 3,
                      childAspectRatio: 5 / 6,
                      crossAxisSpacing: 5,
                      mainAxisSpacing: 10,
                      children: snapshot.data!.docs.map((DocumentSnapshot document) {
                        Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
                        return GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => InformationScreen(
                                        name: data["name"],
                                        description: data["description"],
                                        price: data["price"],
                                        image: data["image"])));
                          },
                          child: GridItemWidget(
                            name: data["name"],
                            description: data["description"],
                            price: data["price"],
                            image: data["image"],
                            onPress: () {
                              storeController.addCart(storeController.userId, data["name"], data["description"],
                                  data["price"], data["image"], document.id);
                              storeController.getStoreCart();
                              FocusManager.instance.primaryFocus?.unfocus();
                            },
                            icon: const Icon(Icons.add_shopping_cart),
                            rating: 4,
                          ),
                        );
                      }).toList(),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
