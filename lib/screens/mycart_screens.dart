import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:testthinh/controllers/authen_controllers.dart';
import 'package:testthinh/controllers/store_controllers.dart';
import 'package:testthinh/widgets/list_item_widgets.dart';

class MyCartScreen extends StatefulWidget {
  const MyCartScreen({Key? key}) : super(key: key);
  @override
  _MyCartScreenState createState() => _MyCartScreenState();
}

class _MyCartScreenState extends State<MyCartScreen> {
  final Stream<QuerySnapshot> _cartStream = FirebaseFirestore.instance
      .collection('Carts')
      .where("userId", isEqualTo: FirebaseAuth.instance.currentUser!.uid)
      .snapshots();
  final storeController = Get.put(StoreController());
  final authenController = Get.put(AuthenController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            width: double.infinity,
            height: 800,
            child: StreamBuilder<QuerySnapshot>(
                stream: _cartStream,
                builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasError) {
                    return const Text('Something went wrong');
                  }

                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(
                        child: CircularProgressIndicator(
                      color: Colors.black,
                      strokeWidth: 2.5,
                    ));
                  }
                  return snapshot.data!.size != 0
                      ? ListView(
                          physics: const BouncingScrollPhysics(),
                          children: snapshot.data!.docs.map((DocumentSnapshot document) {
                            Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
                            return ListItemWidget(
                              name: data["name"],
                              description: data["description"],
                              price: data["price"],
                              image: data["image"],
                              icon: const Icon(
                                Icons.cancel_outlined,
                                color: Colors.red,
                                size: 30,
                              ),
                              onPress: () async {
                                storeController.deleteCart(document.id);
                                EasyLoading.showToast("Đã xoá ${data["name"]}");
                                storeController.getStoreCart();
                              },
                              id: document.id,
                            );
                          }).toList(),
                        )
                      : const Center(
                          child: Icon(
                            Icons.shopping_cart_outlined,
                            color: Colors.red,
                            size: 200,
                          ),
                        );
                }),
          ),
          Row(
            children: [
              const SizedBox(
                width: 50,
              ),
              const Text(
                "Total: ",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
              ),
              const SizedBox(
                width: 30,
              ),
              Expanded(
                child: Container(
                    alignment: Alignment.centerLeft,
                    width: double.infinity,
                    height: 50,
                    child: GetBuilder<StoreController>(
                        init: StoreController(),
                        builder: (controller) => Text(
                              '${controller.total}\$',
                              style: const TextStyle(fontSize: 40, fontWeight: FontWeight.bold, color: Colors.green),
                            ))),
              ),
              SizedBox(
                height: 50,
                width: 150,
                child: TextButton(
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text(
                        "Check out",
                        style: TextStyle(color: Colors.white),
                      ),
                      Icon(
                        Icons.done,
                        color: Colors.greenAccent,
                        size: 30,
                      )
                    ],
                  ),
                  style: TextButton.styleFrom(backgroundColor: Colors.blue),
                ),
              ),
              const SizedBox(
                width: 30,
              ),
            ],
          )
        ],
      ),
    );
  }
}
