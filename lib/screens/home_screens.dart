import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testthinh/controllers/store_controllers.dart';
import 'package:testthinh/widgets/grid_item_widgets.dart';

import 'information_screens.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final Stream<QuerySnapshot> _productStream = FirebaseFirestore.instance.collection('Product').snapshots();
  final storeController = Get.put(StoreController());
  @override
  void initState() {
    //storeController.getStoreUser();
    super.initState();
  }

  //Map map = {"name": "Thịnh", "price": 123};
  final list = [2, 3, 4, 5];
  @override
  Widget build(BuildContext context) {
    // if (map.containsKey("name")) {
    //   print(map["name"]);
    // }
    return Scaffold(
      body: Column(
        children: [
          CarouselSlider(
              items: [
                Image.asset("assets/mobile.jpg"),
                Image.asset("assets/mobile.jpg"),
                Image.asset("assets/mobile.jpg")
              ],
              options: CarouselOptions(
                height: 200,
                aspectRatio: 12 / 9,
                viewportFraction: 0.6,
                initialPage: 0,
                enableInfiniteScroll: true,
                reverse: false,
                autoPlay: true,
                autoPlayInterval: const Duration(seconds: 20),
                autoPlayAnimationDuration: const Duration(milliseconds: 5000),
                autoPlayCurve: Curves.fastOutSlowIn,
                enlargeCenterPage: true,
                // onPageChanged: callbackFunction,
                scrollDirection: Axis.horizontal,
              )),
          SizedBox(
            width: double.infinity,
            height: 650,
            child: StreamBuilder<QuerySnapshot>(
              stream: _productStream,
              builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasError) {
                  return const Text('Something went wrong');
                }
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(
                      child: CircularProgressIndicator(
                    color: Colors.black,
                    strokeWidth: 2.5,
                  ));
                }

                return RefreshIndicator(
                  onRefresh: () {
                    return Future.delayed(
                      const Duration(seconds: 1),
                      () {},
                    );
                  },
                  child: GridView.count(
                    // physics: const BouncingScrollPhysics(),
                    crossAxisCount: 3,
                    childAspectRatio: 5 / 6,
                    crossAxisSpacing: 5,
                    mainAxisSpacing: 10,
                    children: snapshot.data!.docs.map((DocumentSnapshot document) {
                      Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
                      //ProductModel.fromJson(data);
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => InformationScreen(
                                      name: data["name"],
                                      description: data["description"],
                                      price: data["price"],
                                      image: data["image"])));
                        },
                        child: GridItemWidget(
                          name: data["name"],
                          description: data["description"],
                          price: data["price"],
                          image: data["image"],
                          //rating: double.parse(data["rating"].toString()) ?? 0.0,
                          icon: const Icon(Icons.add_shopping_cart),
                          onPress: () {
                            storeController.addCart(storeController.userId, data["name"], data["description"],
                                data["price"], data["image"], document.id);
                            storeController.getStoreCart();
                          },
                          rating: 4.0,
                        ),
                      );
                    }).toList(),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
