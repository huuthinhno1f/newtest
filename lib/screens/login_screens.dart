import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testthinh/controllers/authen_controllers.dart';
import 'package:testthinh/dialogs/otp_dialogs.dart';
import 'package:testthinh/screens/bottombar_screens.dart';
import 'package:testthinh/screens/register_screens.dart';
import 'package:testthinh/widgets/button_widgets.dart';
import 'package:testthinh/widgets/textfild_widgets.dart';

class LoginScreens extends StatefulWidget {
  const LoginScreens({Key? key}) : super(key: key);

  @override
  State<LoginScreens> createState() => _LoginScreensState();
}

class _LoginScreensState extends State<LoginScreens> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();

  bool _selectPhone = true;

  @override
  Widget build(BuildContext context) {
    final authController = Get.put(AuthenController());
    final size = MediaQuery.of(context).size.height;
    // final storeController = Get.put(StoreController());
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        body: Container(
          alignment: Alignment.center,
          width: double.infinity,
          height: size * 1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("LOGIN",
                  style: GoogleFonts.crimsonPro(fontWeight: FontWeight.bold, fontSize: 60, color: Colors.blue, shadows: [
                    const Shadow(
                      offset: Offset(4, 6),
                      blurRadius: 3.0,
                      color: Colors.black,
                    ),
                  ])),
              const SizedBox(
                height: 40,
              ),
              _selectPhone
                  ? TextFildWidget(
                      name: "Email",
                      icon: const Icon(Icons.email),
                      controller: emailController,
                    )
                  : TextFildWidget(
                      name: "Phone Number",
                      icon: const Icon(Icons.phone),
                      controller: phoneController,
                      inputType: TextInputType.phone,
                    ),
              const SizedBox(
                height: 20,
              ),
              _selectPhone
                  ? TextFildWidget(
                      name: "Password",
                      icon: const Icon(Icons.lock),
                      controller: passController,
                      hidepass: true,
                    )
                  : const SizedBox(),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: const [
                  Text("Forgot password?", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold)),
                  SizedBox(
                    width: 250,
                  ),
                  // SizedBox()
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              _selectPhone
                  ? ButtonWidget(
                      name: "LOGIN",
                      onPress: () {
                        authController.signIn("huuthinh@gmail.com", "123456");
                      },
                    )
                  : ButtonWidget(
                      name: "SEND OTP",
                      onPress: () {
                        authController.loginPhone(phoneController.text.trim(), OtpDialog());
                        Get.dialog(const OtpDialog());
                      },
                    ),
              const SizedBox(
                height: 30,
              ),
              const Text(
                "Or login with",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 40,
                    width: 150,
                    child: TextButton(
                        onPressed: () {
                          // Get.dialog(
                          //   LoginPhoneDialog(),
                          // );
                          setState(() {
                            _selectPhone = !_selectPhone;
                          });
                        },
                        style: TextButton.styleFrom(backgroundColor: Colors.grey),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Icon(
                              Icons.phone,
                              color: Colors.black,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              "Phone",
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        )),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  SizedBox(
                    height: 40,
                    width: 150,
                    child: TextButton(
                        onPressed: () {
                          EasyLoading.show(status: "Loading...");
                          authController.signInWithGoogle().then((value) {
                            EasyLoading.dismiss();
                            Get.to(() => const BottomBarScreen());
                          });
                        },
                        style: TextButton.styleFrom(backgroundColor: Colors.red),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              "assets/icongoogle.png",
                              width: 30,
                              height: 30,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            const Text(
                              "Google",
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        )),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("Not a member? "),
                  GestureDetector(
                    onTap: () {
                      Get.to(() => RegisterScreen());
                    },
                    child: const Text(
                      "SignUp now",
                      style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
