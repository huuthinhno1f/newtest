import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testthinh/controllers/store_controllers.dart';

class InformationScreen extends StatelessWidget {
  final String name;
  final String description;
  final int price;
  final String image;
  const InformationScreen({
    Key? key,
    required this.name,
    required this.description,
    required this.price,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final storeController = Get.put(StoreController());
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          "Information",
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              name.toUpperCase(),
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 35, color: Colors.red),
            ),
            const SizedBox(
              height: 20,
            ),
            Hero(
              tag: name,
              child: CachedNetworkImage(
                fit: BoxFit.fill,
                imageUrl: image,
                height: 300,
                width: 300,
                placeholder: (context, url) => const CircularProgressIndicator(
                  strokeWidth: 2,
                  color: Colors.teal,
                ),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              description,
              style: const TextStyle(color: Colors.blue, fontSize: 25),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              "$price\$",
              style: const TextStyle(
                color: Colors.blue,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            SizedBox(
              width: 120,
              height: 40,
              child: TextButton(
                onPressed: () {
                  //storeController.addCart(FirebaseAuth.instance.currentUser!.uid, name, description, price, image);
                },
                child: Row(
                  //crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                      "Add Cart",
                      style: TextStyle(color: Colors.white),
                    ),
                    Icon(
                      Icons.done,
                      color: Colors.greenAccent,
                    )
                  ],
                ),
                style: TextButton.styleFrom(backgroundColor: Colors.blue),
              ),
            )
          ],
        ),
      ),
    );
  }
}
