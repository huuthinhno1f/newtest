import 'dart:io';
import 'dart:ui';

import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:testthinh/controllers/authen_controllers.dart';
import 'package:testthinh/controllers/store_controllers.dart';
import 'package:testthinh/screens/account_screens.dart';
import 'package:testthinh/screens/home_screens.dart';
import 'package:testthinh/screens/mycart_screens.dart';
import 'package:testthinh/screens/search_screens.dart';

class BottomBarScreen extends StatefulWidget {
  const BottomBarScreen({Key? key}) : super(key: key);

  @override
  _BottomBarScreenState createState() => _BottomBarScreenState();
}

class _BottomBarScreenState extends State<BottomBarScreen> {
  List<String> listName = ["Home", "My Cart", "Search", "Account"];
  final storeController = Get.put(StoreController());
  final authenController = Get.put(AuthenController());
  int number = 0;
  int _bottomNavIndex = 0;
  final ImagePicker _picker = ImagePicker();
  String? imagePath;
  XFile? file;
  bool selectFloatButton = false;
  List<IconData> listIcon = [Icons.home, Icons.shopping_cart_outlined, Icons.search, Icons.account_circle_outlined];
  static const List<Widget> _widgetOptions = <Widget>[
    HomeScreen(),
    MyCartScreen(),
    SearchScreens(),
    AccountScreen(),
  ];

  @override
  void initState() {
    setState(() {
      storeController.getStoreCart();
      storeController.getStoreUser();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      onDrawerChanged: (isOpened) {
        FocusManager.instance.primaryFocus?.unfocus();
        if (!isOpened) {
          setState(() {
            number = 0;
          });
        }
      },
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          listName[_bottomNavIndex],
          style: GoogleFonts.cousine(color: Colors.green, fontSize: 35, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: Builder(
          builder: (context) => // Ensure Scaffold is in context
              IconButton(
            icon: const Icon(
              Icons.menu,
              color: Colors.black,
            ),
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        actions: [
          Stack(
            children: [
              IconButton(
                iconSize: 30,
                icon: const Icon(
                  Icons.shopping_cart_outlined,
                  color: Colors.black,
                ),
                onPressed: () {
                  setState(() {
                    _bottomNavIndex = 1;
                  });
                },
              ),
              Positioned(
                top: 0,
                right: 0,
                child: Container(
                    alignment: Alignment.center,
                    height: 20,
                    width: 20,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.red,
                    ),
                    child: GetBuilder<StoreController>(
                      init: StoreController(), // INIT IT ONLY THE FIRST TIME
                      builder: (controller) => Text(
                        '${controller.sizeCart}',
                      ),
                    )),
              )
            ],
          ),
          const SizedBox(
            width: 10,
          ),
          IconButton(
            iconSize: 30,
            icon: const Icon(Icons.power_settings_new_rounded, color: Colors.red),
            onPressed: () {
              authenController.signOut();
            },
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 250,
              child: DrawerHeader(
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Colors.indigoAccent, Colors.green],
                      begin: FractionalOffset(0.2, 0.5),
                      end: FractionalOffset(0.5, 1),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              number = 1;
                            });
                          },
                          child: SizedBox(
                            height: 100,
                            width: 100,
                            child: GetBuilder<StoreController>(
                                builder: (s) => imagePath != null
                                    ? CircleAvatar(
                                        backgroundImage: FileImage(File(imagePath!)),
                                      )
                                    : s.link != null
                                        ? ClipRRect(
                                            borderRadius: BorderRadius.circular(50),
                                            child: CachedNetworkImage(
                                              imageUrl: s.link,
                                              placeholder: (context, url) => const CircularProgressIndicator(),
                                              errorWidget: (context, url, error) => const Icon(Icons.error),
                                              fit: BoxFit.fill,
                                            ),
                                          )
                                        : ClipRRect(
                                            borderRadius: BorderRadius.circular(50),
                                            child: Image.asset(
                                              "assets/noavatar.png",
                                              fit: BoxFit.fill,
                                            ))),
                          ),
                        ),
                        if (number == 1)
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              TextButton(
                                  style: TextButton.styleFrom(backgroundColor: Colors.white60),
                                  onPressed: () {
                                    setState(() {
                                      number = 2;
                                    });
                                  },
                                  child: const Text(
                                    "Chỉnh sửa ảnh",
                                    style: TextStyle(color: Colors.black, fontSize: 12),
                                  )),
                              TextButton(
                                  style: TextButton.styleFrom(backgroundColor: Colors.white60),
                                  onPressed: () {
                                    setState(() {
                                      number = 0;
                                    });
                                  },
                                  child: const Text(
                                    "Huỷ",
                                    style: TextStyle(color: Colors.black, fontSize: 12),
                                  )),
                            ],
                          ))
                        else
                          const SizedBox(),
                        if (number == 2)
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              TextButton(
                                  style: TextButton.styleFrom(backgroundColor: Colors.white60),
                                  onPressed: () async {
                                    file = await _picker.pickImage(source: ImageSource.gallery);
                                    if (file != null) {
                                      setState(() {
                                        imagePath = file!.path;
                                        number = 3;
                                      });
                                    } else {
                                      //print("chuwa chon");
                                    }
                                  },
                                  child: const Text(
                                    "Chọn từ thư mục",
                                    style: TextStyle(color: Colors.black, fontSize: 12),
                                  )),
                              TextButton(
                                  style: TextButton.styleFrom(backgroundColor: Colors.white60),
                                  onPressed: () {
                                    setState(() async {
                                      file = await _picker.pickImage(source: ImageSource.camera);
                                      if (file != null) {
                                        setState(() {
                                          imagePath = file!.path;
                                          number = 3;
                                        });
                                      } else {
                                        print("chuwa chon");
                                      }
                                    });
                                  },
                                  child: const Text(
                                    "Chụp ảnh",
                                    style: TextStyle(color: Colors.black, fontSize: 12),
                                  )),
                            ],
                          ))
                        else
                          const SizedBox(),
                        if (number == 3)
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              TextButton(
                                  style: TextButton.styleFrom(backgroundColor: Colors.white60),
                                  onPressed: () async {
                                    await storeController.uploadFile(file!.path, storeController.getRandomString(20));
                                    await storeController.getStoreUser();
                                    setState(() {
                                      number = 0;
                                    });
                                  },
                                  child: const Text(
                                    "Lưu",
                                    style: TextStyle(color: Colors.black, fontSize: 12),
                                  )),
                              TextButton(
                                  style: TextButton.styleFrom(backgroundColor: Colors.white60),
                                  onPressed: () {
                                    setState(() {
                                      file == null;
                                      storeController.getStoreUser();
                                      number = 0;
                                    });
                                  },
                                  child: const Text(
                                    "Huỷ",
                                    style: TextStyle(color: Colors.black, fontSize: 12),
                                  )),
                            ],
                          ))
                        else
                          const SizedBox(),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    GetBuilder<StoreController>(
                        init: StoreController(),
                        builder: (s) {
                          return Text(
                            "${s.name}",
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          );
                        }),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                      "${FirebaseAuth.instance.currentUser!.email}",
                      style: const TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w300),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              title: const Text('Home'),
              leading: const Icon(
                Icons.home,
                color: Colors.green,
                size: 30,
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Account'),
              leading: const Icon(
                Icons.account_circle_outlined,
                color: Colors.green,
                size: 30,
              ),
              onTap: () {
                // Get.back();
                // storeController.makeGetRequest();
                // authenController.creatDatabase();
              },
            ),
            ListTile(
              title: const Text('My Cart'),
              leading: const Icon(
                Icons.shopping_cart_outlined,
                color: Colors.green,
                size: 30,
              ),
              onTap: () {
                setState(() {
                  _bottomNavIndex = 1;
                });
                Get.back();
              },
            ),
            ListTile(
              title: const Text('Search'),
              leading: const Icon(
                Icons.search,
                color: Colors.green,
                size: 30,
              ),
              onTap: () {
                setState(() {
                  _bottomNavIndex = 2;
                });
                Get.back();
              },
            ),
            ListTile(
              title: const Text('Logout'),
              leading: const Icon(
                Icons.power_settings_new_rounded,
                color: Colors.red,
                size: 30,
              ),
              onTap: () {
                authenController.signOut();
              },
            ),
          ],
        ),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_bottomNavIndex),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: selectFloatButton ? Colors.white : Colors.black87,
        child: Icon(
          selectFloatButton ? Icons.dark_mode : Icons.wb_sunny,
          color: selectFloatButton ? Colors.black87 : Colors.white,
        ),
        onPressed: () {
          setState(() {
            selectFloatButton = !selectFloatButton;
          });
        },
        //params
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: AnimatedBottomNavigationBar(
        backgroundColor: selectFloatButton ? Colors.white : Colors.black87,
        icons: listIcon, iconSize: 30, activeColor: Colors.blue,
        elevation: 0,
        activeIndex: _bottomNavIndex,
        inactiveColor: selectFloatButton ? Colors.black12 : Colors.grey,
        gapLocation: GapLocation.center,
        notchSmoothness: NotchSmoothness.verySmoothEdge,
        splashSpeedInMilliseconds: 300,
        onTap: (index) => setState(() => _bottomNavIndex = index),
        //other params
      ),
    );
  }
}
