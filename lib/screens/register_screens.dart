import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testthinh/controllers/authen_controllers.dart';
import 'package:testthinh/widgets/button_widgets.dart';
import 'package:testthinh/widgets/textfild_widgets.dart';

class RegisterScreen extends StatelessWidget {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();
  final TextEditingController confirmController = TextEditingController();
  RegisterScreen({Key? key}) : super(key: key);
  final authController = Get.put(AuthenController());

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            "REGISTER",
          ),
        ),
        body: Container(
          width: double.infinity,
          height: 1000,
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "REGISTER",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 50, color: Colors.blue),
              ),
              const SizedBox(
                height: 40,
              ),
              TextFildWidget(
                name: "Enter email",
                icon: const Icon(Icons.email),
                controller: emailController,
              ),
              const SizedBox(
                height: 10,
              ),
              TextFildWidget(
                name: "Enter password",
                icon: const Icon(Icons.lock),
                controller: passController,
                hidepass: true,
              ),
              const SizedBox(
                height: 10,
              ),
              TextFildWidget(
                name: "Confirm password",
                icon: const Icon(Icons.lock),
                controller: confirmController,
                hidepass: true,
              ),
              const SizedBox(
                height: 20,
              ),
              ButtonWidget(
                name: "Register",
                onPress: () {
                  authController.signUp(
                    emailController.text.trim(),
                    confirmController.text.trim(),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
