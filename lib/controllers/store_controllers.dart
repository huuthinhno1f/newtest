import 'dart:io';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart' as firebase_core;
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:testthinh/models/carts_models.dart';

class StoreController extends GetxController {
  CollectionReference products = FirebaseFirestore.instance.collection('Product');
  CollectionReference carts = FirebaseFirestore.instance.collection('Carts');
  CollectionReference users = FirebaseFirestore.instance.collection('Users');

  final userId = FirebaseAuth.instance.currentUser!.uid;
  var sizeCart = 0.obs;
  String? linkImage;
  List<CartModel> listCart = [];

  List<int> listTotal = [];
  int get total {
    int result = 0;
    if (listTotal.isNotEmpty) result = listTotal.reduce((value, element) => value + element);
    return result;
  }

  Future<void> getStoreCart() async {
    listTotal = [];
    final QuerySnapshot qSnap = await carts.where("userId", isEqualTo: userId).get();
    sizeCart = qSnap.docs.length.obs;

    if (qSnap.docs.isEmpty) {
    } else {
      for (var doc in qSnap.docs) {
        listTotal.add(doc["price"]);
      }
    }
    update();
  }

  dynamic link, name;

  Future<void> getStoreUser() async {
    await users.where("userId", isEqualTo: userId).get().then((QuerySnapshot querySnapshot) {
      for (var doc in querySnapshot.docs) {
        downloadURLExample(doc["imagePath"]);
        link = doc["linkImage"];
        name = doc["name"];
      }
    });

    update();
  }

  Future<void> addCart(String userId, String name, String description, int price, String image, String cartId) async {
    carts
        .add({
          'cartId': cartId,
          'userId': userId,
          'name': name,
          'description': description,
          'price': price,
          'image': image,
        })
        .then((value) => EasyLoading.showToast("Đã thêm"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  Future<void> addUser(String userId, String name, String imagePath, String linkImage) async {
    users
        .doc(userId)
        .set({
          'userId': userId,
          'name': name,
          'imagePath': imagePath,
          'linkImage': linkImage,
        })
        .then((value) => EasyLoading.showToast("Đã thêm"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  Future<void> deleteCart(String cartId) async {
    return carts
        .doc(cartId)
        .delete()
        .then((value) => print("User Deleted"))
        .catchError((error) => print("Failed to delete user: $error"));
  }

  Future<void> uploadFile(String filePath, String imagePath) async {
    addUser(userId, "Đào Hữu Thịnh", imagePath, "");
    File file = File(filePath);

    try {
      await firebase_storage.FirebaseStorage.instance.ref("$userId/$imagePath").putFile(file);
    } on firebase_core.FirebaseException catch (e) {
      // e.g, e.code == 'canceled'
    }
  }

  static const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  final Random _rnd = Random();

  String getRandomString(int length) =>
      String.fromCharCodes(Iterable.generate(length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  Future<void> updateUser(String linkImage) {
    return users
        .doc(userId)
        .update({'linkImage': linkImage})
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  Future<void> downloadURLExample(String nameImage) async {
    String downloadURL = await firebase_storage.FirebaseStorage.instance.ref("$userId/$nameImage").getDownloadURL();
    updateUser(downloadURL);
  }
}
