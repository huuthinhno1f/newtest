import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:testthinh/screens/bottombar_screens.dart';
import 'package:testthinh/screens/login_screens.dart';

class AuthenController extends GetxController {
  FirebaseAuth auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }

  Future<void> signIn(String email, String password) async {
    EasyLoading.show(status: "Loading...");
    if (email == "" || password == "") {
      EasyLoading.showError("Login Error!");
    } else {
      try {
        await auth
            .signInWithEmailAndPassword(
          email: email,
          password: password,
        )
            .then((value) {
          Get.to(() => const BottomBarScreen());

          EasyLoading.showSuccess("Login Success");
        });
      } on FirebaseAuthException catch (e) {
        if (e.code == 'user-not-found') {
          //print('No user found for that email.');
          EasyLoading.showError("No user found for that email.");
        } else if (e.code == 'wrong-password') {
          // print('Wrong password provided for that user.');
          EasyLoading.showError("Wrong password provided for that user.");
        } else {
          EasyLoading.showError("Login Error!");
        }
      }
    }
  }

  Future<void> signUp(String email, String password) async {
    EasyLoading.show(status: "Loading...");
    try {
      await auth.createUserWithEmailAndPassword(email: email, password: password).then((value) {
        EasyLoading.showSuccess("Register Success");
      });
    } on FirebaseAuthException catch (e) {
      EasyLoading.showError("Login Error!");
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> signOut() async {
    await EasyLoading.show(status: "Logout...");
    await auth.signOut().then((value) => EasyLoading.dismiss());
    await googleSignIn.signOut();
    Get.off(LoginScreens());
  }

  Future<void> loginPhone({String? phoneNumber} String otp) async {
    await auth.verifyPhoneNumber(
      phoneNumber: '+84$phoneNumber',
      timeout: const Duration(seconds: 60),
      verificationCompleted: (PhoneAuthCredential credential) async {
        // ANDROID ONLY!

        // Sign the user in (or link) with the auto-generated credential
        print("vô đây 1");
        await auth.signInWithCredential(credential);
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        print("đã hết thời gian");
      },
      codeSent: (String verificationId, int? forceResendingToken) async {
        String smsCode = otp;
        print("ma code $smsCode");
        print("vô đây 2");
        // Create a PhoneAuthCredential with the code
        PhoneAuthCredential credential = PhoneAuthProvider.credential(verificationId: verificationId, smsCode: smsCode);

        // Sign the user in (or link) with the credential
        await auth.signInWithCredential(credential).then((value) {
          print("ok");
          Get.to(() => const BottomBarScreen());
        }).onError((error, stackTrace) {
          print("khong dang nhap duoc $error");
        });
      },
      verificationFailed: (FirebaseAuthException e) {
        if (e.code == 'invalid-phone-number') {
          print('The provided phone number is not valid.');
        }
      },
    );
  }

// Future<void> creatDatabase() async {
//   var databasesPath = await getDatabasesPath();
//   String path = join(databasesPath, 'demo.db');
//   Database database = await openDatabase(path, version: 1, onCreate: (Database db, int version) async {
//     // When creating the db, create the table
//     await db.execute('CREATE TABLE Test (id INTEGER PRIMARY KEY, name TEXT, value INTEGER, num REAL)');
//   });
//   // await database.transaction((txn) async {
//   //   int id1 = await txn.rawInsert('INSERT INTO Test(name, value, num) VALUES("some name", 1234, 456.789)');
//   //   print('inserted11: $id1');
//   //   int id2 = await txn.rawInsert('INSERT INTO Test(name, value, num) VALUES(?, ?, ?)', ['thịnh', 08687, 8.48744]);
//   //   print('inserted21: $id2');
//   //   int id3 = await txn.rawInsert('INSERT INTO Test(name, value, num) VALUES(?, ?, ?)', ['vanb', 48411, 8.1454]);
//   //   print('inserted31: $id3');
//   // });
//   List<Map> list = await database.rawQuery('SELECT * FROM Test');
//   List<Map> expectedList = [
//     {'name': 'updated name', 'id': 1, 'value': 9876, 'num': 456.789},
//     {'name': 'another name', 'id': 2, 'value': 12345678, 'num': 3.1416}
//   ];
//   print("Thịnh database $list");
//   print(expectedList);
// }
}
